1
00:00:00,34 --> 00:00:00,49
well,

2
00:00:00,49 --> 00:00:02,69
let's return to the XD bit for a bit

3
00:00:02,7 --> 00:00:03,76
No pun intended

4
00:00:04,14 --> 00:00:09,34
Originally the non execute NX bit was an AMD invention

5
00:00:09,34 --> 00:00:12,44
and intel eventually picked it up as the XD execute

6
00:00:12,44 --> 00:00:13,16
disable bit,

7
00:00:13,64 --> 00:00:16,46
but it's more commonly referred to as the NX bit

8
00:00:17,04 --> 00:00:20,35
And the whole usage is that it's designed to help

9
00:00:20,35 --> 00:00:26,21
firmware operating systems and virtualization systems builds exploit mitigations to

10
00:00:26,21 --> 00:00:29,82
mark areas that are typically abused by Attackers such as

11
00:00:29,82 --> 00:00:32,79
the stack and the heat has not execute Herbal so

12
00:00:32,79 --> 00:00:35,1
the Attackers can't just put code in there and jump

13
00:00:35,1 --> 00:00:35,45
to it

14
00:00:35,46 --> 00:00:38,36
Fundamentally what software makers are trying to achieve is a

15
00:00:38,36 --> 00:00:42,57
security policy called right X or execute And you can

16
00:00:42,57 --> 00:00:44,42
be rideable or it can be executed but you can't

17
00:00:44,42 --> 00:00:46,26
be both as mentioned before

18
00:00:46,26 --> 00:00:49,02
The segmentation did actually already have this policy,

19
00:00:49,02 --> 00:00:51,18
but people don't use that to protect things

20
00:00:51,18 --> 00:00:55,11
And with the X-86-64 having hobbled the segmentation,

21
00:00:55,11 --> 00:00:56,06
they couldn't use it now,

22
00:00:56,06 --> 00:00:56,96
even if they wanted to

23
00:00:57,44 --> 00:00:58,26
Also,

24
00:00:58,26 --> 00:01:02,02
some software projects such as packs had created W X

25
00:01:02,02 --> 00:01:05,93
or X permissions before hardware support was even available in

26
00:01:05,93 --> 00:01:09,33
X 86 but this came at a significant performance cost

27
00:01:09,33 --> 00:01:10,39
And so these days,

28
00:01:10,4 --> 00:01:14,25
anyone who can does use the NX or XD bit

29
00:01:14,34 --> 00:01:16,82
when it comes to what takes precedence,

30
00:01:16,83 --> 00:01:20,75
if you ever see a pt PD or Pt Pt

31
00:01:20,84 --> 00:01:22,6
with the extra bit set to one,

32
00:01:22,7 --> 00:01:25,65
that means that the subsequent memory is going to be

33
00:01:25,65 --> 00:01:26,56
non execute able

34
00:01:27,14 --> 00:01:30,01
And specifically as the emu is walking through the page

35
00:01:30,01 --> 00:01:30,72
tables,

36
00:01:30,73 --> 00:01:33,96
it is the first XD bit which is encountered,

37
00:01:33,97 --> 00:01:37,25
which takes precedence over all subsequent ones

38
00:01:37,28 --> 00:01:40,2
So another way of saying that is the permissions will

39
00:01:40,2 --> 00:01:42,35
be the most restrictive as possible

40
00:01:43,14 --> 00:01:46,48
So going back to our shared memory view from before

41
00:01:46,49 --> 00:01:49,62
if the XD bit was set right here,

42
00:01:49,63 --> 00:01:52,72
it would mean everything downstream is all going to be

43
00:01:52,72 --> 00:01:53,57
non executable,

44
00:01:53,58 --> 00:01:56,33
it doesn't matter if you eventually reach this and it

45
00:01:56,33 --> 00:01:56,7
says,

46
00:01:56,7 --> 00:01:59,25
oh executable is fine or this inexcusable is fine

47
00:01:59,84 --> 00:02:04,24
This particular page is not going to be excusable because

48
00:02:04,24 --> 00:02:05,52
all the way back here,

49
00:02:05,53 --> 00:02:07,23
it was marked as non executable

50
00:02:07,3 --> 00:02:11,03
So everything here on this entire page table is not

51
00:02:11,03 --> 00:02:11,66
excusable

52
00:02:12,44 --> 00:02:14,69
When the process switches over to here,

53
00:02:14,69 --> 00:02:15,78
you might have,

54
00:02:15,94 --> 00:02:17,4
this is that is excusable,

55
00:02:17,4 --> 00:02:18,3
that's excusable,

56
00:02:18,3 --> 00:02:19,25
that's excusable,

57
00:02:19,64 --> 00:02:20,83
that's execute herbal

58
00:02:20,84 --> 00:02:25,19
And so these particular pages could be executed Ble but

59
00:02:25,2 --> 00:02:26,73
even though it says execute herbal

60
00:02:26,73 --> 00:02:27,26
Excusable

61
00:02:27,26 --> 00:02:29,01
Excusable when it gets to here,

62
00:02:29,01 --> 00:02:29,92
it's not excusable

63
00:02:29,92 --> 00:02:33,48
And so again these pages are non excusable when interpreted

64
00:02:33,48 --> 00:02:34,62
through this page table

65
00:02:34,63 --> 00:02:36,41
So in this entire picture,

66
00:02:36,41 --> 00:02:39,46
all of this is non executable and this is excusable

67
00:02:39,46 --> 00:02:44,0
only because everything upstream from it is all having the

68
00:02:44,01 --> 00:02:45,74
execute disable bit not set

69
00:02:46,0 --> 00:02:49,43
If the processor tries to do any instruction fetches for

70
00:02:49,44 --> 00:02:52,19
execution from one of these non executable pages,

71
00:02:52,19 --> 00:02:53,96
that does result in a page fault

72
00:02:54,34 --> 00:02:57,98
Microsoft's marketing term for the utilization of the XD bit

73
00:02:57,99 --> 00:03:00,76
is known as data execution prevention or depth,

74
00:03:01,14 --> 00:03:03,84
also called hardware debt because they have a separate thing

75
00:03:03,84 --> 00:03:05,21
they call software depth,

76
00:03:05,22 --> 00:03:08,5
but that refers to a completely different technology about structured

77
00:03:08,5 --> 00:03:11,74
exception handler sanity checking that really has nothing to do

78
00:03:11,74 --> 00:03:12,35
with XD

79
00:03:12,36 --> 00:03:16,9
So either sometimes called depth or more properly called hardware

80
00:03:16,9 --> 00:03:19,62
depth refers to use of this hardware bit in the

81
00:03:19,62 --> 00:03:22,46
page tables to protect against code execution

82
00:03:24,34 --> 00:03:24,76
All right

83
00:03:24,76 --> 00:03:28,19
So how do we know whether a particular process processor

84
00:03:28,19 --> 00:03:29,56
supports the NX bit?

85
00:03:29,94 --> 00:03:33,67
Well back to this MSR again again and in bid

86
00:03:33,67 --> 00:03:36,92
11 of this MSR is the enablement for whether or

87
00:03:36,92 --> 00:03:39,07
not this hardware supports the NX

88
00:03:39,08 --> 00:03:41,47
And furthermore this is again the thing where it said

89
00:03:41,47 --> 00:03:41,66
Oh,

90
00:03:41,66 --> 00:03:44,8
but you have to check bit 20 orbit 29

91
00:03:44,81 --> 00:03:45,96
If we go back to CP

92
00:03:45,96 --> 00:03:48,65
I did we see that for this particular input value

93
00:03:48,65 --> 00:03:49,48
for ADX?

94
00:03:49,48 --> 00:03:52,36
Bit 20 is the thing that actually says this hardware

95
00:03:52,36 --> 00:03:54,18
supports execute disabled,

96
00:03:54,19 --> 00:03:57,93
so then that e for register MSR is the thing

97
00:03:57,93 --> 00:04:00,28
which an operating system would have to actually set it

98
00:04:00,28 --> 00:04:01,56
in order for it to be used

